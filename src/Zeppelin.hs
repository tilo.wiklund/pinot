{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module Zeppelin where

import qualified Data.Default as D
import Data.Text (Text)
import qualified Data.Text as T
import Data.Aeson
import Data.Monoid ((<>))
import qualified Data.ByteString.Lazy as B
import qualified Data.HashMap.Lazy as H
import Control.Lens hiding ((.=))
import Utils
import qualified Text.Pandoc.Class as P
import qualified Text.Pandoc.Builder as P
import qualified Text.Pandoc.Definition as P
import qualified Text.Pandoc.Writers.Markdown as P
import qualified Text.Pandoc.Writers.HTML as P
import qualified Text.Pandoc.Readers.HTML as P
import qualified Notebook as N

data ZeppelinNotebook = ZN { _znAngularObjects :: Maybe (H.HashMap Text Value)
                           , _znConfig         :: Maybe ZeppelinConfig
                           , _znParagraphs     :: [ZeppelinParagraph]
                           , _znName           :: Text
                           , _znId             :: Maybe Text
                           , _znInfo           :: Maybe ZeppelinInfo }
  deriving Show

instance D.Default ZeppelinNotebook where
  def = ZN Nothing Nothing [] "" Nothing Nothing

newtype ZeppelinConfig = ZC String
  deriving Show

instance D.Default ZeppelinConfig where
  def = ZC ""

newtype ZeppelinInfo = ZI ()
  deriving Show

instance D.Default ZeppelinInfo where
  def = ZI ()


data ZeppelinMessage = ZM { _zmType :: Text
                          , _zmData :: Text }
  deriving Show

data ZeppelinResult = ZR { _zrCode :: Text
                         , _zrMsg :: [ZeppelinMessage] }
  deriving Show

instance D.Default ZeppelinResult where
  def = ZR "" []

instance FromJSON ZeppelinMessage where
  parseJSON = withObject "Message" $ \v -> ZM
    <$> v .: "type"
    <*> v .: "data"

instance FromJSON ZeppelinResult where
  parseJSON = withObject "Results" $ \v -> ZR
    <$> v .: "code"
    <*> v .: "msg"

data ZeppelinParagraph = ZP { _zpFocus                    :: Maybe Bool
                            , _zpStatus                   :: Maybe Text
                            , _zpApps                     :: Maybe [Value]
                            , _zpConfig                   :: Maybe Value
                            , _zpProgressUpdateIntervalMs :: Maybe Double
                            , _zpSettings                 :: Maybe Value
                            , _zpText                     :: Text
                            , _zpJobName                  :: Maybe Text
                            , _zpResults                  :: Maybe ZeppelinResult
                            , _zpDateUpdated              :: Maybe Text
                            , _zpDateCreated              :: Maybe Text
                            , _zpDateStarted              :: Maybe Text
                            , _zpDateFinished             :: Maybe Text
                            , _zpHashKey                  :: Maybe Text
                            , _zpId                       :: Maybe Text
                            , _zpErrorMessage             :: Maybe Value }
  deriving Show

makeLenses ''ZeppelinMessage
makeLenses ''ZeppelinResult
makeLenses ''ZeppelinNotebook
makeLenses ''ZeppelinParagraph

instance ToJSON ZeppelinMessage where
  toEncoding zm = pairs ( "type"    .= (zm^.zmType)
                          <> "data" .= (zm^.zmData) )
  toJSON zm = objectMaybe [ "type" .= (zm^.zmType)
                          , "data" .= (zm^.zmData) ]

instance ToJSON ZeppelinResult where
  toEncoding zr = pairs ( "code" .= (zr^.zrCode)
                          <> "msg" .= (zr^.zrMsg) )
  toJSON zr = objectMaybe  [ "code" .= (zr^.zrCode)
                           , "msg" .= (zr^.zrMsg) ]

instance D.Default ZeppelinParagraph where
  def = ZP Nothing Nothing Nothing Nothing Nothing Nothing "" Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

instance ToJSON ZeppelinParagraph where
  toEncoding zp = pairs ( "focus"           .=? (zp^.zpFocus)
                          <> "status"       .=? (zp^.zpStatus)
                          <> "apps"         .=? (zp^.zpApps)
                          <> "config"       .=? (zp^.zpConfig)
                          <> "progressUpdateIntervalMs"
                                            .=? (zp^.zpProgressUpdateIntervalMs)
                          <> "settings"     .=? (zp^.zpSettings)
                          <> "text"         .=  (zp^.zpText)
                          <> "jobName"      .=? (zp^.zpJobName)
                          <> "results"      .=? (zp^.zpResults)
                          <> "dateUpdated"  .=? (zp^.zpDateUpdated)
                          <> "dateCreated"  .=? (zp^.zpDateCreated)
                          <> "dateStarted"  .=? (zp^.zpDateStarted)
                          <> "dateFinished" .=? (zp^.zpDateFinished)
                          <> "$$hashKey"    .=? (zp^.zpHashKey)
                          <> "id"           .=? (zp^.zpId)
                          <> "errorMessage" .=? (zp^.zpErrorMessage) )

  toJSON zp = objectMaybe [ "focus"        .=? (zp^.zpFocus)
                          , "status"       .=? (zp^.zpStatus)
                          , "apps"         .=? (zp^.zpApps)
                          , "config"       .=? (zp^.zpConfig)
                          , "progressUpdateIntervalMs"
                                           .=? (zp^.zpProgressUpdateIntervalMs)
                          , "settings"     .=? (zp^.zpSettings)
                          , "text"         .=  (zp^.zpText)
                          , "jobName"      .=? (zp^.zpJobName)
                          , "results"      .=? (zp^.zpResults)
                          , "dateUpdated"  .=? (zp^.zpDateUpdated)
                          , "dateCreated"  .=? (zp^.zpDateCreated)
                          , "dateStarted"  .=? (zp^.zpDateStarted)
                          , "dateFinished" .=? (zp^.zpDateFinished)
                          , "$$hashKey"    .=? (zp^.zpHashKey)
                          , "id"           .=? (zp^.zpId)
                          , "errorMessage" .=? (zp^.zpErrorMessage) ]

instance FromJSON ZeppelinParagraph where
  parseJSON = withObject "Paragraph" $ \v -> ZP
    <$> v .:? "focus"
    <*> v .:? "status"
    <*> v .:? "apps"
    <*> v .:? "config"
    <*> v .:? "progressUpdateIntervalMs"
    <*> v .:? "settings"
    <*> v .:  "text"
    <*> v .:? "jobName"
    <*> v .:? "results"
    <*> v .:? "dateUpdated"
    <*> v .:? "dateCreated"
    <*> v .:? "dateStarted"
    <*> v .:? "dateFinished"
    <*> v .:? "$$hashKey"
    <*> v .:? "id"
    <*> v .:? "errorMessage"

instance ToJSON ZeppelinNotebook where
  toEncoding zn = pairs ( "angularObjects" .=? (zn^.znAngularObjects)
                          <> "config"      .=? (zn^.znConfig)
                          <> "paragraphs"  .=  (zn^.znParagraphs)
                          <> "name"        .=  (zn^.znName)
                          <> "id"          .=? (zn^.znId)
                          <> "info"        .=? (zn^.znInfo))

  toJSON zn = objectMaybe [ "angularObjects" .=? (zn^.znAngularObjects)
                          , "config"         .=? (zn^.znConfig)
                          , "paragraphs"     .=  (zn^.znParagraphs)
                          , "name"           .=  (zn^.znName)
                          , "id"             .=? (zn^.znId)
                          , "info"           .=? (zn^.znInfo) ]

instance FromJSON ZeppelinNotebook where
  parseJSON = withObject "ZeppelinNotebook" $ \v -> ZN
    <$> v .:? "angularObjects"
    <*> v .:? "config"
    <*> v .: "paragraphs"
    <*> v .: "name"
    <*> v .:? "id"
    <*> v .:? "info"

instance ToJSON ZeppelinConfig where
  toEncoding (ZC looknfeel) = pairs ("looknfeel" .= looknfeel)
  toJSON (ZC looknfeel) = object [ "looknfeel" .= looknfeel ]

instance FromJSON ZeppelinConfig where
  parseJSON = withObject "ZeppelinConfig" $ \v -> ZC <$> (v .: "looknfeel")

instance ToJSON ZeppelinInfo where
  toEncoding _ = pairs mempty
  toJSON _ = object []

instance FromJSON ZeppelinInfo where
  parseJSON = withObject "ZeppelinInfo" (const (return (ZI ())))

fromByteString :: B.ByteString -> Either String ZeppelinNotebook
fromByteString = eitherDecode

toByteString :: ZeppelinNotebook -> B.ByteString
toByteString = encode

toZPLanguage :: N.Language -> T.Text
toZPLanguage N.Java             = "java"
toZPLanguage N.R                = "R"
toZPLanguage N.Scala            = "spark"
toZPLanguage N.Python           = "python"
toZPLanguage N.Haskell          = "haskell"
toZPLanguage (N.Spark N.R)      = "sparkr"
toZPLanguage (N.Spark N.Scala)  = "spark"
toZPLanguage (N.Spark N.Python) = "pyspark"
toZPLanguage N.Markdown         = "md"
toZPLanguage (N.Spark l)        = toZPLanguage l
toZPLanguage (N.Other x)        = x

fromZPLanguage :: T.Text -> N.Language
fromZPLanguage "java"    = N.Java
fromZPLanguage "r"       = N.R
fromZPLanguage "python"  = N.Python
fromZPLanguage "haskell" = N.Haskell
fromZPLanguage "sparkr"  = N.Spark N.R
fromZPLanguage "spark"   = N.Spark N.Scala
fromZPLanguage "pyspark" = N.Spark N.Python
fromZPLanguage "md"      = N.Markdown
fromZPLanguage x         = N.Other x

fromNotebook :: N.Notebook -> ZeppelinNotebook
fromNotebook nb = defWith [ znName .~ (nb^.N.nName)
                          , znParagraphs .~ map toZParagraph (nb^.N.nCommands) ]
  where toZParagraph nc = defWith [ zpText .~ addLang (nc^.N.cLanguage) (nc^.N.cCommand)
                                  , zpResults .~ (toZPResult <$> (nc^.N.cState)) ]
        addLang l c = T.unlines [ T.cons '%' (toZPLanguage l), c ]
        toZPResult (N.RError (N.DPlain msg)) = ZR "ERROR" [ZM "TEXT" msg]
        toZPResult (N.RError _) = error "Complex error outputs currently unsupported in Zeppelin writer"
        toZPResult (N.RSuccess (N.DRich bs)) = ZR "SUCCESS" [ZM "HTML" (either (error . show) id (P.runPure (P.writeHtml5String D.def (P.Pandoc (P.Meta D.def) (P.toList bs)))))]
        toZPResult (N.RSuccess (N.DHTML html)) = ZR "SUCCESS" [ZM "HTML" html]
        toZPResult (N.RSuccess (N.DPlain txt)) = ZR "SUCCESS" [ZM "TEXT" txt]
        toZPResult (N.RSuccess N.DTable {}) = ZR "SUCCESS" [ ZM "TEXT" "<table currently omitted in Zeppelin output>" ]
--          where tableBody = either (error . show) id (P.runPure (P.writeHtml5String D.def (P.Pandoc (P.Meta D.def) [P.Table P.def P.def [] ] )))

toNotebook :: ZeppelinNotebook -> N.Notebook
toNotebook zn = N.N (zn^.znName) (map toCommand (zn^.znParagraphs))
  where toCommand zp =
          let (langTag, rawCommand) = splitLangTag (zp^.zpText)
              result = parseResult <$> (zp^.zpResults) in
          case fmap fromZPLanguage langTag of
            -- Yes, this really is what zeppelin does...
            Nothing   -> N.C (N.Spark N.Scala) rawCommand result False False
            Just lang -> N.C lang rawCommand result False False
        splitLangTag unparsedCommand =
          if (unparsedCommand `safeIndex` 0) == Just '%'
          then let (x:xs) = T.lines unparsedCommand
               in (Just (T.stripEnd . T.tail $ x), T.unlines xs)
          else (Nothing, unparsedCommand)
        parseResult (ZR "SUCCESS" msgs) = N.RSuccess (N.DRich (collapseRich msgs))
        parseResult (ZR "ERROR" msgs) = N.RError (N.DPlain (collapseSimple msgs))
        parseResult _ = error "I don't know how to parse this result"
        collapseSimple msgs = mconcat (map simpleMessage msgs)
        collapseRich msgs = P.fromList (mconcat (map richMessage msgs))
        simpleMessage (ZM "TEXT" t) = t
        simpleMessage (ZM "HTML" t) = either (error . show) id (P.runPure (P.writePlain D.def (either (error . show) id (P.runPure (P.readHtml D.def t)))))
        simpleMessage _ = error "I don't know how to parse this message"
        richMessage (ZM "TEXT" t) = [P.Plain [P.Str t]]
        richMessage (ZM "HTML" t) = unPandoc (either (error . show) id (P.runPure (P.readHtml D.def t)))
        richMessage _ = error "I don't know how to parse this message"
        unPandoc (P.Pandoc _ bs) = bs

-- toPandoc :: ZeppelinNotebook -> P.Pandoc
-- toPandoc z = P.doc $ foldMap (P.codeBlock . unpack . pText) (znParagraphs z)

-- fromPandoc :: P.Pandoc -> ZeppelinNotebook
-- fromPandoc = undefined
