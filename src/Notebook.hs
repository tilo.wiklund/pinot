{-# LANGUAGE TemplateHaskell, DeriveGeneric #-}
module Notebook where

import Data.Text (Text)
import Control.Lens
import qualified Text.Pandoc.Builder as P
import qualified Data.Hashable as Hash

import GHC.Generics (Generic)

data Language = Java
              | R
              | Scala
              | Python
              | Haskell
              | Spark Language
              | Markdown
              | Other Text
              deriving (Eq, Show, Generic)

baseLanguage :: Language -> Language
baseLanguage (Spark l) = baseLanguage l
baseLanguage x = x

instance Hash.Hashable Language

data Notebook = N { _nName     :: Text
                  , _nCommands :: [Command] }
              deriving (Eq, Show)

data Command = C { _cLanguage       :: Language
                 , _cCommand        :: Text
                 , _cState          :: Maybe Result
                 , _cResultHidden   :: Bool
                 , _cCommandHidden  :: Bool }
             deriving (Eq, Show)

data Result = RSuccess ResultDisplay
            | RError ResultDisplay
            deriving (Eq, Show)

data ResultDisplay = DRich  P.Blocks
                   | DHTML  Text
                   | DPlain Text
                   | DTable (Maybe [Text]) [[Text]]
                   deriving (Eq, Show)

makeLenses ''Notebook
makeLenses ''Command

success :: Command -> Maybe ResultDisplay
success (C _ _ (Just (RSuccess p)) _ _) = Just p
success _ = Nothing
