{-# LANGUAGE OverloadedStrings #-}
module Formats where

import qualified Data.ByteString.Lazy as B hiding (putStrLn)

import Control.Lens

import Zeppelin as Z
import Databricks as D
import Notebook as N
import Pandoc as P
import Utils

type SourceFormat = [FilePath] -> String -> B.ByteString -> IO (Either String [(String, N.Notebook)])
type TargetFormat = [FilePath] -> [(String, N.Notebook)] -> IO ([(String, B.ByteString)])
type NotebookFormat = (SourceFormat, TargetFormat)

databricksDBCSource :: SourceFormat
databricksDBCSource _ f x = case over (_Right . each . _2) D.toNotebook (fromByteStringArchive x) of
                              Left err -> error err
                              Right ns -> return $ Right ns

databricksJSONSource :: SourceFormat
databricksJSONSource _ f x = return $ (singleton . D.toNotebook) <$> D.fromByteString x
  where singleton y = [(f, y)]

zeppelinSource :: SourceFormat
zeppelinSource _ f x = return $ (singleton . Z.toNotebook) <$> Z.fromByteString x
  where singleton y = [(f, y)]

databricksJSONTarget :: TargetFormat
databricksJSONTarget ep ns = return $ over (each . _2) compile ns
  where compile = D.toByteString . D.fromNotebook

zeppelinTarget :: TargetFormat
zeppelinTarget ep ns = return $ over (each . _1) (swapExtension ".json") $ over (each . _2) compile ns
  where compile = Z.toByteString . Z.fromNotebook

sequenceSnd :: [(a, IO b)] -> IO [(a, b)]
sequenceSnd = sequence . map sequence

markdownTarget :: TargetFormat
markdownTarget ep = sequenceSnd . over (each . _1) (swapExtension ".md") . over (each . _2) compile
  where compile = P.toMarkdown ep . P.fromNotebook P.DefaultStyle

markdownTargetKaTeX :: TargetFormat
markdownTargetKaTeX ep = sequenceSnd . over (each . _1) (swapExtension ".md") . over (each . _2) compile
  where compile = P.toMarkdownKaTeX ep . P.fromNotebook P.DefaultStyle

htmlTarget :: TargetFormat
htmlTarget ep = sequenceSnd . over (each . _1) (swapExtension ".html") . over (each . _2) compile
  where compile = P.toHtml ep . P.fromNotebook P.DefaultStyle

pandocTarget :: TargetFormat
pandocTarget ep = sequenceSnd . over (each . _1) (swapExtension ".pandoc") . over (each . _2) compile
  where compile = P.toNative ep . P.fromNotebook P.DefaultStyle

pandocJSONTarget :: TargetFormat
pandocJSONTarget ep = sequenceSnd . over (each . _1) (swapExtension ".json") . over (each . _2) compile
  where compile = P.toJSON ep . P.fromNotebook P.DefaultStyle

jupyterTarget :: TargetFormat
jupyterTarget ep = sequenceSnd . over (each . _1) (swapExtension ".ipynb") . over (each . _2) compile
  where compile = P.toJupyter ep . P.fromNotebook P.DefaultStyle

jupyterBookTarget :: TargetFormat
jupyterBookTarget ep = sequenceSnd . over (each . _1) (swapExtension ".ipynb") . over (each . _2) compile
  where compile = P.toJupyterBook ep . P.fromNotebook P.JupyterBookStyle

mdBookTarget :: TargetFormat
mdBookTarget ep = sequenceSnd . over (each . _1) (swapExtension ".md") . over (each . _2) compile
  where compile = P.toMDBook ep . P.fromNotebook P.MDBookStyle

zeppelinFormat = (zeppelinSource, zeppelinTarget)
databricksJSONFormat = (databricksJSONSource, databricksJSONTarget)
