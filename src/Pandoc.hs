{-# LANGUAGE OverloadedStrings #-}
module Pandoc where

import Prelude hiding (truncate)

import qualified Data.ByteString.Lazy as B hiding (pack)

import qualified Text.Pandoc.Builder as P
import Text.Pandoc.Builder ((<>))
import qualified Text.Pandoc.Options as P
import qualified Text.Pandoc.Readers.Markdown as P
import qualified Text.Pandoc.Writers as P
import qualified Text.Pandoc.Writers.Markdown as P
import qualified Text.Pandoc.Writers.HTML as P
import qualified Text.Pandoc.Writers.Native as P
import qualified Text.Pandoc.Readers as P
import qualified Text.Pandoc.Class as P
import qualified Text.Pandoc.Walk as P
import qualified Text.Pandoc.Logging as P

import qualified Data.Hashable as Hash
import qualified Data.HashMap.Strict as Hash

import Data.Default (def)

import Data.Char (isSpace)

import qualified Data.Map as Map (Map, fromList)

import Control.Lens

import Notebook as N
import Utils

import Debug.Trace

import Data.Text as T
import Data.Text.Encoding as E

toMarkdownLanguage :: N.Language -> T.Text
toMarkdownLanguage (N.Spark x) = toMarkdownLanguage x
toMarkdownLanguage (N.Other t) = t
toMarkdownLanguage  Markdown   = "md"
toMarkdownLanguage  Java       = "java"
toMarkdownLanguage  R          = "r"
toMarkdownLanguage  Python     = "python"
toMarkdownLanguage  Haskell    = "haskell"
toMarkdownLanguage  Scala      = "scala"

fromMarkdownLanguage :: String -> N.Language
fromMarkdownLanguage "md" = N.Markdown
fromMarkdownLanguage "markdown" = N.Markdown
fromMarkdownLanguage "java" = N.Java
fromMarkdownLanguage "r" = N.R
fromMarkdownLanguage "python" = N.Python
fromMarkdownLanguage "haskell" = N.Haskell
fromMarkdownLanguage x = N.Other (T.pack x)

markdownCellExtensions :: P.Extensions
markdownCellExtensions = P.extensionsFromList [P.Ext_latex_macros, P.Ext_tex_math_dollars, P.Ext_backtick_code_blocks, P.Ext_pipe_tables, P.Ext_tex_math_double_backslash, P.Ext_raw_html {-, P.Ext_tex_math_single_backslash -}]

truncate :: Int -> a -> [a] -> [a]
truncate 0 _ [] = []
truncate 0 a _  = [a]
truncate _ _ [] = []
truncate n a (x:xs) = x : truncate (n-1) a xs

counts :: (Eq a, Hash.Hashable a) => [a] -> Hash.HashMap a Int
counts xs = Hash.fromListWith (+) [ (x, 1) | x <- xs ]

mostFrequent :: (Eq a, Hash.Hashable a) => [a] -> Maybe a
mostFrequent = pickUniqueMax . Hash.foldrWithKey poke Nothing . counts
  where poke k v Nothing = Just ([k], v)
        poke k v (Just (k's, v')) | v'  > v = Just (k's, v)
                                  | v' == v = Just (k : k's, v)
                                  | v'  < v = Just ([k], v)
        pickUniqueMax (Just ([k], _)) = Just k
        pickUniqueMax _ = Nothing


mostFrequentLanguage :: N.Notebook -> Maybe N.Language
mostFrequentLanguage n = mostFrequent $ Prelude.map (^.N.cLanguage) (n^.N.nCommands)

mapKeys :: (Eq b, Hash.Hashable b) => (a -> b) -> Hash.HashMap a c -> Hash.HashMap b c
mapKeys f = Hash.fromList . Prelude.map (\(x, y) -> (f x, y)) . Hash.toList

mostFrequentBaseLanguage :: N.Notebook -> Maybe N.Language
mostFrequentBaseLanguage n = mostFrequent $ Prelude.map baseLanguage $ Prelude.map (^.N.cLanguage) (n^.N.nCommands)

data PandocStyle = DefaultStyle
                 | JupyterBookStyle
                 | MDBookStyle
                 deriving (Eq, Show)

python2715Meta :: P.MetaValue
python2715Meta = P.MetaMap $ Map.fromList [("kernelspec", kernelspec), ("language_info", languageInfo)]
  where kernelspec = P.MetaMap $ Map.fromList [
          ("display_name", P.MetaString "Python 2"),
          ("language", P.MetaString "python"),
          ("name", P.MetaString "python2") ]
        codemirrorMode = P.MetaMap $ Map.fromList [
          ("name", P.MetaString "ipython"),
          ("version", P.MetaString "2") ]
        languageInfo = P.MetaMap $ Map.fromList [
          ("codemirror_mode", codemirrorMode),
          ("file_extension", P.MetaString ".py"),
          ("mimetype", P.MetaString "text/x-python"),
          ("name", P.MetaString "python"),
          ("nbconvert_exporter", P.MetaString "python"),
          ("pygments_lexer", P.MetaString "ipython2"),
          ("version", P.MetaString "2.7.15")]

scala212Meta :: P.MetaValue
scala212Meta = P.MetaMap $ Map.fromList [("kernelspec", kernelspec), ("language_info", languageInfo)]
  where kernelspec = P.MetaMap $ Map.fromList [
          ("display_name", P.MetaString "Scala (2.12)"),
          ("language", P.MetaString "scala"),
          ("name", P.MetaString "scala212") ]
        languageInfo = P.MetaMap $ Map.fromList [
          ("codemirror_mode", P.MetaString "text/x-scala"),
          ("file_extension", P.MetaString ".scala"),
          ("mimetype", P.MetaString "text/x-scala"),
          ("name", P.MetaString "scala"),
          ("nbconvert_exporter", P.MetaString "script"),
          ("pygments_lexer", P.MetaString "scala"),
          ("version", P.MetaString "2.12.10")]

fromNotebook :: PandocStyle -> N.Notebook -> P.Pandoc
fromNotebook style nb = setLanguageMeta $ P.setTitle title $ P.doc $ foldMap block (nb^.nCommands)
  where title = P.text (nb^.nName)
        setLanguageMeta = case mostFrequentBaseLanguage nb of
           Just Scala  -> P.setMeta "jupyter" scala212Meta
           Just Python -> P.setMeta "jupyter" python2715Meta
           _ -> id
        block c | c^.cLanguage == N.Markdown =
                  let parsed = P.runPure $ P.readMarkdown ( def { P.readerExtensions = markdownCellExtensions } ) (c^.cCommand)
                      P.Pandoc _ bs = either (error . show) id parsed
                  in P.singleton $ P.Div ("", ["cell", "markdown"], []) (P.toList (blocks bs))
                | otherwise =
                  let lang = toMarkdownLanguage $ c^.cLanguage
                      code = if c^.cCommandHidden || (T.null (c^.cCommand))
                             then []
                             else [P.CodeBlock ("", [lang], []) (c^.cCommand)]
--                             else [P.CodeBlock ("", [lang], []) ("%%" <> lang <> "\n" <> c^.cCommand)]
                      result = if c^.cResultHidden
                               then []
                               else case N.success c of
                                      Nothing -> []
                                      Just (N.DRich _) ->
                                        [P.Div ("", ["output", "execute_result"], [("execution_count", "1")]) [P.CodeBlock ("unsuported", [], []) "Rich text/markdown output formats currently unsupported"]]
                                      Just (N.DTable hs css) ->
                                        let table = P.doc $ P.simpleTable
                                              [ P.singleton (P.Plain [P.Str h]) | h <- maybe [] id hs ]
                                              [ [P.singleton (P.Plain [P.Str c]) | c <- cs] | cs <- css ]
                                            html = fromRight . P.runPure $ P.writeHtml5String def table
                                        in [P.Div ("", ["output", "execute_result"] <> if style == MDBookStyle then ["tabular_result"] else [], [("execution_count", "1")]) [P.RawBlock (P.Format "html") html]]
                                      Just (N.DHTML pd) ->
                                        [P.Div ("", ["output", "execute_result"] <> if style == MDBookStyle then ["html_result"] else [], [("execution_count", "1")]) [P.RawBlock (P.Format "html") pd]]
                                      Just (N.DPlain pd) ->
                                        [P.Div ("", ["output", "execute_result"] <> if style == MDBookStyle then ["plain_result"] else [], [("execution_count", "1")]) [P.CodeBlock ("", [], []) pd]]
                      scrollAttrs = let forceScroll = [("scrolled", "true")] ++
                                          if style == JupyterBookStyle then [("tags", "[\"output_scroll\"]")] else []
                                        forceNoScroll = [("scrolled", "false")]
                                        autoScroll = [("scrolled", "auto")] ++
                                          if style == JupyterBookStyle then [("tags", "[\"output_scroll\"]")] else []
                                    in case N.success c of
                                         Just (N.DTable _ css) -> if Prelude.length css > 15 then forceScroll else forceNoScroll
                                         Just (N.DHTML _) -> autoScroll
                                         Just (N.DPlain pd) -> if Prelude.length (T.lines pd) > 30 then forceScroll else forceNoScroll
                                         _ -> forceNoScroll
                  in P.singleton $ P.Div ("", ["cell", "code"], [("execution_count", "1")] ++ scrollAttrs) (code ++ result)

fromRight :: Show a => Either a b -> b
fromRight (Right b) = b
fromRight (Left b) = error $ "This error is not handled: " ++ (show b)

notebookWriteOpts = def { P.writerExtensions = (P.extensionsFromList [P.Ext_hard_line_breaks, P.Ext_raw_tex, P.Ext_tex_math_dollars, P.Ext_latex_macros, P.Ext_fenced_divs, P.Ext_raw_attribute] <> P.githubMarkdownExtensions), P.writerWrapText = P.WrapPreserve }
markdownWriteOpts = def { P.writerExtensions = (P.extensionsFromList [P.Ext_hard_line_breaks, P.Ext_raw_tex, P.Ext_tex_math_dollars, P.Ext_latex_macros, P.Ext_raw_attribute] <> P.githubMarkdownExtensions), P.writerWrapText = P.WrapPreserve }
mdbookWriteOpts = def { P.writerExtensions = (P.extensionsFromList [P.Ext_hard_line_breaks, P.Ext_raw_tex, P.Ext_tex_math_double_backslash, P.Ext_latex_macros, P.Ext_raw_html, P.Ext_native_divs] <> P.githubMarkdownExtensions), P.writerWrapText = P.WrapPreserve }

toMarkdown :: [FilePath] -> P.Pandoc -> IO B.ByteString
-- toMarkdown = B.pack . P.writeMarkdown (def { P.writerExtensions = P.githubMarkdownExtensions })
toMarkdown ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          P.writeMarkdown markdownWriteOpts p

toMDBook :: [FilePath] -> P.Pandoc -> IO B.ByteString
-- toMarkdown = B.pack . P.writeMarkdown (def { P.writerExtensions = P.githubMarkdownExtensions })
toMDBook ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          P.writeMarkdown mdbookWriteOpts p

toMarkdownNotebook :: [FilePath] -> P.Pandoc -> IO B.ByteString
-- toMarkdown = B.pack . P.writeMarkdown (def { P.writerExtensions = P.githubMarkdownExtensions })
toMarkdownNotebook ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          P.writeMarkdown notebookWriteOpts p

toMarkdownKaTeX :: [FilePath] -> P.Pandoc -> IO B.ByteString
-- toMarkdown = B.pack . P.writeMarkdown (def { P.writerExtensions = P.githubMarkdownExtensions })
toMarkdownKaTeX ep = toMarkdown ep . katexify
  where katexify = P.walk makeDisplayMath
        makeDisplayMath (P.Math _ x) = P.Math P.DisplayMath x
        makeDisplayMath x = x

toHtml :: [FilePath] -> P.Pandoc -> IO B.ByteString
toHtml ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          P.writeHtml5String def p

toNative :: [FilePath] -> P.Pandoc -> IO B.ByteString
toNative ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          P.writeNative def p

toJSON :: [FilePath] -> P.Pandoc -> IO B.ByteString
toJSON ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          P.writeJSON def p

jupyterExtensions :: P.Extensions
jupyterExtensions = P.extensionsFromList [P.Ext_tex_math_dollars, P.Ext_latex_macros, P.Ext_raw_html]

toJupyter :: [FilePath] -> P.Pandoc -> IO B.ByteString
toJupyter ep p = B.fromStrict . E.encodeUtf8 . fromRight <$> P.runIO act
  where act = do
          P.setVerbosity P.INFO
          rp <- P.getResourcePath
          P.setResourcePath $ ep ++ rp
          -- NOTE: Shoule be id, but somehow now...
          p' <- P.readJSON def =<< P.writeJSON def p
          P.writeIpynb def { P.writerExtensions = jupyterExtensions } p'

toJupyterBook :: [FilePath] -> P.Pandoc -> IO B.ByteString
toJupyterBook ep = toJupyter ep . (P.walk indentMath) . (P.walk spaceMaths)
  where spaceMaths :: P.Block -> P.Block
        spaceMaths (P.Plain xs) = P.Plain (Prelude.concatMap spaceMath xs)
        spaceMaths (P.Para  xs) = P.Para  (Prelude.concatMap spaceMath xs)
        spaceMaths x = x
        spaceMath (P.Math P.DisplayMath x) = [P.LineBreak, P.LineBreak, P.Math P.DisplayMath x, P.LineBreak, P.LineBreak]
        spaceMath x = [x]
        indentMath :: P.Inline -> P.Inline
        indentMath (P.Math P.DisplayMath x) = P.Math P.DisplayMath (doubleSpaceIndentLines x)
        indentMath x = x
        doubleSpaceIndentLines = T.unlines . Prelude.map doubleSpaceIndent . T.lines
        doubleSpaceIndent l = "  " <> T.dropWhile isSpace l
