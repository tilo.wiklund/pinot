Software somehwat in the same vein as [pandoc](http://pandoc.org/), but for notebooks (Jupyter, Zeppelin, ...) rather than markup formats. Currently only has very rudimentary support for [Zeppelin](https://zeppelin.apache.org/) and [Databricks](https://community.cloud.databricks.com/) as well as outputting markdown.

Copy of the following repository:
[https://gitlab.com/tilo.wiklund/pinot](https://gitlab.com/tilo.wiklund/pinot)

If you install [Stack](https://haskellstack.org/) you should be able to simply (in the source code folder) run:
```
stack setup
stack build
```
to build the project.

Run `stack exec pinot` for basic usage information.

## Examples

### JupyterBook

 The following example converts a databricks archive to a (collection of) Jupyter(-book compatible) notebook(s):
```
stack exec pinot -- --from databricks --to jupyter-book databricks_notebook.dbc -o output_folder
```
where `databricks_notebook.dbc` is what you get by selecting `DBC Archive` in the `export` section of databricks.

### mdbook

Install [mdbook](https://github.com/rust-lang/mdBook/releases) and start a new mdbook project
```
mkdir my-book
cd my-book
mdbook init .
mkdir src/contents
```

Generate mdbook-compatible markdown files
```
stack exec pinot -- --from databricks --to mdbook databricks_notebook.dbc -o src/contents
```

Either write `SUMMARY.md` manually or generate it along the lines of 
```
cd src
find contents -iname '*.md' -type f | sort -h | while read f; do echo "- ["$(basename $f .md)"](./$f)"; done > SUMMARY.md
cd ..
```

Now generate the html
```
mdbook build
```

The final results are in the `book` directory of the book root directory.

## FAQ

### I want LaTeX support in mdbook

If you need LaTeX support modify `book.toml` by appending
```
[output.html]
mathjax-support = true
```

### I want scrollable outputs in mdbook 

If you want scrollable output results add [the following css](https://gitlab.com/tilowiklund/pinot/-/snippets/2076137/raw/master/scroll-mdbook-outputs.css?inline=false) to the root directory of the book and add the following to the `[output.html]` section of `book.toml`
```
additional-css = ["scroll-mdbook-outputs.css"]
```

### I'm getting errors related to missing files

Sometimes databricks dbc archives do not contain all resource files (e.g. images uploaded to DB but only referred to in Markdown).

If you're outputting to a Pandoc-based format (ipynb/jupyter, Markdown, HTML) you can use the `-R` option to give an additional path to search for images (and other resources)

Example: If pinot failed because of a missing file 'foo/bar/baz.jpg' do
```
mkdir extra-resources
mkdir -p extra-resources/foo/bar
cp path/to/image.jpg extra-resources/foo/bar/baz.jpg
stack exec pinot -- ... -R extra-resources
```

### Find missing images and broken image links

There is a small python script [available here](https://gitlab.com/tilowiklund/imgsoup).
