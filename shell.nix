{ pkgs ? import <nixpkgs> {}, ghc ? pkgs.ghc }:

pkgs.haskell.lib.buildStackProject {
  name = "pinot";
  inherit ghc;
  buildInputs = with pkgs; [ glibcLocales zlib ];
  LOCALE_ARCHIVE_2_27 = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG = "en_GB.UTF-8";
  TMPDIR = "/tmp";
}
